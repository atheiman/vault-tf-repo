#!/bin/bash
# based on the example from https://www.terraform.io/docs/providers/external/data_source.html

set -e

# jq will ensure that the values are properly quoted
# and escaped for consumption by the shell.
eval "$(jq -r '@sh "CMD=\(.cmd); VAULT_ADDR=\(.vault_addr); VAULT_TOKEN=\(.vault_token)"')"

# If vault_addr and vault_token are passed in as query parameters, they should
# be exported as environment variables for the real command to use.
if [ "${VAULT_ADDR}" != "null" ]; then
    export VAULT_ADDR="${VAULT_ADDR}"
fi

if [ "${VAULT_TOKEN}" != "null" ]; then
    export VAULT_TOKEN="${VAULT_TOKEN}"
fi

set +e
bash -c "${CMD}" >&2
retval=$?
set -e

# Safely produce a JSON object containing the result value.
# jq will ensure that the value is properly quoted
# and escaped to produce a valid JSON string.
jq -n --arg retval "$retval" '{"exit_status":$retval}'
# variable "vault_addr" {
#   description = "Origin URL of the Vault server. This is a URL with a scheme, a hostname and a port but with no path."
# }

# variable "vault_token" {
#   description = "Vault token that will be used by Terraform to authenticate."
# }

variable "command" {
  type        = "string"
  description = "Arguments to append to the vault command, should not include the vault command itself"
}

variable "trigger" {
  type        = "string"
  default     = ""
  description = "Additional trigger to cause this command to be re-executed. Specify '$${uuid()}' to run on every apply."
}

variable "depends_on" {
  type        = "string"
  default     = ""
  description = "Specify an output from another vault_exec module call to depend on it."
}

variable "only_if" {
  type        = "string"
  default     = "true"
  description = "Vault command will only be executed if this command returns an exit code of 0."
}

data "external" "only_if" {
  program = ["bash", "${path.module}/exit_status.sh"]

  query = {
    cmd = "${var.only_if}"

    # vault_addr  = "${var.vault_addr}"
    # vault_token = "${var.vault_token}"
  }
}

resource "null_resource" "execute_vault_cmd" {
  # If var.only_if returns an exit code of 0, then run this resource
  # NOTE: This will usually cause this resource to be deleted from the tfstateon
  # subsequent runs, but that isn't really a problem since it will be re-added
  # in the future if it needs to run again.
  count = "${data.external.only_if.result.exit_status == 0 ? 1 : 0}"

  triggers {
    var_trigger = "${var.trigger}"
    command     = "${var.command}"
  }

  provisioner "local-exec" {
    # A lot of vault operations take a few seconds to complete after the command
    # returns, so force a small sleep
    command = "sleep 5 && vault ${var.command}"

    # environment {
    #   VAULT_ADDR  = "${var.vault_addr}"
    #   VAULT_TOKEN = "${var.vault_token}"
    # }
  }
}

# This output is only really useful to make one vault_exec call depend on another
# see https://github.com/hashicorp/terraform/issues/1178#issuecomment-105613781
output "noop" {
  value = "${uuid()}"
}

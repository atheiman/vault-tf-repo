# variable "vault_addr" {
#   description = "Origin URL of the Vault server. This is a URL with a scheme, a hostname and a port but with no path."
# }

# variable "vault_token" {
#   description = "Vault token that will be used by Terraform to authenticate."
# }

variable "audit_log_path" {
  type        = "string"
  description = "The path to where the audit log will be written."
  default     = "stdout"
}

resource "vault_audit" "file" {
  path        = "file"
  type        = "file"
  description = "Write audit log to file"

  options = {
    path = "${var.audit_log_path}"
  }
}

module "disable_default_kv" {
  source  = "../vault_exec"
  command = "secrets disable secret/"

  # vault_addr  = "${var.vault_addr}"
  # vault_token = "${var.vault_token}"
}

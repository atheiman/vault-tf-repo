# variable "vault_addr" {
#   description = "Origin URL of the Vault server. This is a URL with a scheme, a hostname and a port but with no path."
# }

# variable "vault_token" {
#   description = "Vault token that will be used by Terraform to authenticate."
# }

variable "environment" {
  description = "Environment to configure in Vault (dev, qa, prod)."
}

resource "vault_auth_backend" "approle" {
  type        = "approle"
  path        = "approle/${var.environment}"
  description = "${var.environment} approle auth method"
}

resource "vault_mount" "kv" {
  path        = "secret/${var.environment}"
  type        = "kv"
  description = "${var.environment} KV secrets engine"

  options = {
    version = "2"
  }
}

resource "vault_mount" "database" {
  path = "database/${var.environment}"
  type = "database"

  description = "${var.environment} database secrets engine"
}

data "template_file" "admin_policy" {
  template = "${file("${path.module}/templates/admin-policy.hcl")}"

  vars {
    environment      = "${var.environment}"
    kv_secrets       = "${vault_mount.kv.path}"
    database_secrets = "${vault_mount.database.path}"
  }
}

resource "vault_policy" "admin" {
  name   = "${var.environment}/admin"
  policy = "${data.template_file.admin_policy.rendered}"
}

output "approle_auth_path" {
  value = "${vault_auth_backend.approle.path}"
}

output "kv_secrets_path" {
  value = "${vault_mount.kv.path}"
}

output "database_secrets_path" {
  value = "${vault_mount.database.path}"
}

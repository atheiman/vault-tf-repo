# https://www.vaultproject.io/docs/concepts/policies.html
# https://www.vaultproject.io/api/
# https://learn.hashicorp.com/vault/identity-access-management/iam-policies#root-protected-api-endpoints

path "auth/${environment}/*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}

path "${kv_secrets}/*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}

path "${database_secrets}/*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}

path "/sys/leases*" {
  capabilities = [ "create", "read", "update", "delete", "list", "sudo" ]
}

path "/sys/audit*" {
  capabilities = [ "create", "read", "update", "delete", "list", "sudo" ]
}

path "/sys/auth*" {
  capabilities = [ "create", "read", "update", "delete", "list", "sudo" ]
}

path "/sys/config/auditing*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}

path "/sys/config/cors*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}

path "/sys/config/ui*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}

path "/sys/internal/ui/mounts*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}

path "/sys/mounts*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}

path "/sys/policy*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}

path "/sys/policies*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}

path "/sys/remount*" {
  capabilities = [ "create", "read", "update", "delete", "list" ]
}

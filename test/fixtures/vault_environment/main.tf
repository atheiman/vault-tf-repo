provider "vault" {}

module "kitchen-terraform-environment" {
  source      = "../../../modules/vault_environment"
  environment = "kitchen-terraform-environment"
}

control 'vault_base' do
  describe command('vault version') do
    its('exit_status') { should eq 0 }
    its('stderr') { should cmp '' }
  end

  describe vault_command('status -format=json') do
    its('sealed') { should cmp false }
    its('version') { should match(/1\.0\.\d+/) }
    its('cluster_name') { should match(/vault-cluster-\w+/) }
    its('stderr') { should cmp '' }
  end

  describe vault_command('secrets list --format=json') do
    its('json') { should_not include('secret/') }
    its('stderr') { should cmp '' }
  end

  describe vault_command('audit list --format=json') do
    its('file/') { should include('type' => 'file', 'options' => { 'path' => 'stdout' }) }
    its('stderr') { should cmp '' }
  end
end

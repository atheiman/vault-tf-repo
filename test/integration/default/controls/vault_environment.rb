control 'vault_environment' do
  describe vault_command('auth list --format=json') do
    its('json') { should include('approle/kitchen-terraform-environment/') }
    its('stderr') { should cmp '' }
  end

  describe vault_command('secrets list --detailed --format=json') do
    its('secret/kitchen-terraform-environment/') do
      should include('type' => 'kv', 'options' => { 'version' => '2' })
    end
    its('database/kitchen-terraform-environment/') { should include('type' => 'database') }
    its('stderr') { should cmp '' }
  end

  describe vault_command('secrets list --detailed --format=json') do
    its('secret/kitchen-terraform-environment/') do
      should include('type' => 'kv', 'options' => { 'version' => '2' })
    end
    its('database/kitchen-terraform-environment/') do
      should include(
        'type' => 'database',
        'local' => false,
        'seal_wrap' => false
      )
    end
    its('stderr') { should cmp '' }
  end

  describe vault_command('policy read kitchen-terraform-environment/admin') do
    crudl = '"create", "read", "update", "delete", "list"'
    [
      'path "auth/kitchen-terraform-environment/\*" {\s+capabilities = \[ ' + crudl + ' \]\s+}',
      'path "secret/kitchen-terraform-environment/\*" {\s+capabilities = \[ ' + crudl + ' \]\s+}',
      'path "database/kitchen-terraform-environment/\*" {\s+capabilities = \[ ' + crudl + ' \]\s+}',
      'path "/sys/leases\*" {\s+capabilities = \[ ' + crudl + ', "sudo" \]\s+}',
      'path "/sys/audit\*" {\s+capabilities = \[ ' + crudl + ', "sudo" \]\s+}',
      'path "/sys/auth\*" {\s+capabilities = \[ ' + crudl + ', "sudo" \]\s+}',
      'path "/sys/config/auditing\*" {\s+capabilities = \[ ' + crudl + ' \]\s+}',
      'path "/sys/config/cors\*" {\s+capabilities = \[ ' + crudl + ' \]\s+}',
      'path "/sys/config/ui\*" {\s+capabilities = \[ ' + crudl + ' \]\s+}',
      'path "/sys/internal/ui/mounts\*" {\s+capabilities = \[ ' + crudl + ' \]\s+}',
      'path "/sys/mounts\*" {\s+capabilities = \[ ' + crudl + ' \]\s+}',
      'path "/sys/policy\*" {\s+capabilities = \[ ' + crudl + ' \]\s+}',
      'path "/sys/policies\*" {\s+capabilities = \[ ' + crudl + ' \]\s+}',
      'path "/sys/remount\*" {\s+capabilities = \[ ' + crudl + ' \]\s+}'
    ].each do |policy_element|
      its('stdout') do
        should cmp(Regexp.new(policy_element))
      end
    end
    its('stderr') { should cmp '' }
  end
end

# Vault Terraform Repo

Example showing loading resources into Vault using [the Terraform provider](https://www.terraform.io/docs/providers/vault/index.html).

## Notes

- any policy files at [`templates/policies/*.hcl`](./templates/policies/) will be templated and loaded into vault by [`policies.tf`](./policies.tf)
- [`apps.tf`](./apps.tf) creates approle roles and policies for apps using the [`app`](./app/) module
- there is a [`vault_exec`](./vault_exec/) module for running vault cli commands
  - when using this, `terraform apply -parallelism=1` can be helpful so that vault commands dont get run before another vault command they depend on.

## Try it

Start vault dev server locally in a terminal session

```shell
vault server -dev
```

In another terminal, plan / apply the terraform

```shell
export VAULT_ADDR=http://localhost:8200
terraform plan
terraform apply
```

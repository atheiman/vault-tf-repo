variable "vault_addr" {
  description = "Origin URL of the Vault server. This is a URL with a scheme, a hostname and a port but with no path."
}

variable "vault_token" {
  description = "Vault token that will be used by Terraform to authenticate."
}
